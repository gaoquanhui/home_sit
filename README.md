# home_sit

#### 介绍
家庭文件下载站，部署简单文件易用

#### 软件架构
纯属自己搭建


#### 安装教程

1.  下载文件到网页目录
2.  打开js/index.js
3.  修改window.open（"url"）url为您的网址

#### 使用说明

1.  千万部署到服务器使用
2.  file.html只是一个建议文件站，建议使用hfs替换

#### 参与贡献

1.  gaoquanhui

